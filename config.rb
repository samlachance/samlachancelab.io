# Activate and configure extensions
activate :autoprefixer do |prefix|
  prefix.browsers = "last 2 versions"
end

# Layouts
# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

function toggleMOTD() {
  var motd = document.getElementById("motd");
  var expireAt = new Date(motd.getAttribute("data-expire-at"));

  if(new Date > expireAt) {
    motd.style.display = "none";
  }
}
